#include <stdio.h>
#include <sys/socket.h>
#include <stdlib.h>
#include <netinet/in.h>
#include <string.h>
#include "/usr/local/mysql/include/mysql.h"
#include "constants.h"
char *server = "127.0.0.1";
char *user = "root";
char *password = "";
char *database = "SaccoProject";
char * optionHandler = "";
int memberId;
char * userName = "";
int connectionSet = 0;
int totalContributionVariable = 0;
int totalLoanPayBack = 0;
int actualBorrowedLoan = 0;
char i, j;
MYSQL *conn;
char initiation_message[1024] = "Hello, welcome to the family SACCO.\nEnter Command to continue: or type help to view the commands list\n";
void clientSender(char message[1024], int socketS);
void handleClientResponse(char clientMessage[1024], int socketS);
void handleContributionAdd(char contributionString[1024], int socketS);
void addContributionToTempFile(char data[1024]);
void handleIdeaAdd(char ideaString[1024], int socketS);
void addIdeaToTempFile(char data[1024]);
void handleLoanRequest(char loanString[1024], int socketS);
void addLoanToTempFile(char data[1024]);
void handleLoanPayment(char loanPaymentString[1024], int socketS);
int checkIfUserExists();
char * concatinateString(char * string1, char * string2);
int loanStatus();
int totalContributions();
int getLoanPaymentDetails();

int main(int argc, char const *argv[])
{
    /* initialize connection handler */
    /*
        Connection steps
        1.create socket
        2.bind the socket to an address
        3.listen for incoming requests
        4.accept incoming requests
        5.send content to the client
    */
    int server_socket, new_socket, valread;
    struct sockaddr_in address;
    int addrlen = sizeof(address);

    // Creating socket file descriptor
    if ((server_socket = socket(AF_INET, SOCK_STREAM, 0)) == 0)
    {
        perror("socket failed");
        exit(EXIT_FAILURE);
    }

    //defining address structure
    address.sin_family = AF_INET;
    address.sin_addr.s_addr = INADDR_ANY;
    address.sin_port = htons( PORT );

    // binding the socket
    if (bind(server_socket, (struct sockaddr *)&address, sizeof(address))<0)
    {
        perror("bind failed");
        exit(EXIT_FAILURE);
    }

    //listening to one client //five for testing
    if (listen(server_socket, 1) < 0)
    {
        perror("listen");
        exit(EXIT_FAILURE);
    }

    if ((new_socket = accept(server_socket, (struct sockaddr *)&address,
           (socklen_t*)&addrlen))<0)
        {
            perror("acceptance error: ");
            exit(EXIT_FAILURE);
        }

    //making server listen forever
    for(;;){
        //read content from client
        char userInput[1024] = {0};
        valread = read( new_socket , userInput, sizeof(userInput));


        //validate user
        if (connectionSet == 1){
            if(strcmp(optionShowMenu, userInput) == 0){

                clientSender(initiation_message, new_socket);
                optionHandler = optionDefault;

            }else if(strcmp(optionHandler, optionDefault) == 0){

                //if user has just opened the application
                handleClientResponse(userInput, new_socket);

            }else if(strcmp(optionHandler, optionContributionAdd) == 0){

                //assuming user selected adding new contribution
                handleContributionAdd(userInput, new_socket);

            }else{
                optionHandler = optionDefault;
                clientSender(errorMessageDefault, new_socket);
                close(new_socket);
            }
        }else{
            char * enhancedInput = strcat(userInput, "\n");

            if(checkIfUserExists(enhancedInput) == 1){
                connectionSet = 1;
                optionHandler = optionDefault;
                clientSender(initiation_message, new_socket);
            }else{
                clientSender(errorMessageDefaultLogin, new_socket);
            }
        }
    }
    close(new_socket);

    return 0;
}

char * concatinateString(char * string1, char * string2){
    char * str3 = (char *) malloc(1 + strlen(string1)+ strlen(string2) );
    strcpy(str3, string1);
    strcat(str3, string2);
    return str3;
}

int checkIfUserExists(char * usernameInput){
    int userExists = 0;
    conn = mysql_init(NULL);
   
   /* Connect to database */
   if (!mysql_real_connect(conn, server,
         user, password, database, 3307, NULL, 0)) {
      fprintf(stderr, "%s\n", mysql_error(conn));
      return(0);
   }

    /* send SQL query */
    char query_template[] = "SELECT * FROM members WHERE username = '%s'";
    char query[sizeof usernameInput + sizeof query_template];

    //remove new line character
    usernameInput[strcspn(usernameInput, "\n")] = 0;
    sprintf(query, query_template, usernameInput);

   if (mysql_query(conn, query)) {
      fprintf(stderr, "%s\n", mysql_error(conn));
      return(0);
   }

   MYSQL_RES *result = mysql_store_result(conn);
   if (result == NULL) 
    {
      fprintf(stderr, "%s\n", mysql_error(conn));
      return(0);
    }

    MYSQL_ROW row;
    MYSQL_FIELD *field;
    int num_fields = mysql_num_fields(result);
    unsigned int name_field;
    char *field_name = "memberId";

    char *headers[num_fields];
    for(unsigned int i = 0; (field = mysql_fetch_field(result)); i++) {
        headers[i] = field->name;
        if (strcmp(field_name, headers[i]) == 0) {
            name_field = i;
        }
    }

    while ((row = mysql_fetch_row(result))) {
        //do something with row[name_field]
        if(row[name_field] != NULL){
            memberId = (int) strtol(row[name_field], NULL, 10);
            userExists = 1;
        }
    }

   /* Release memory used to store results and close connection */
   mysql_free_result(result);
   mysql_close(conn);
   return userExists;
}

int loanStatus(){
    int loanStatusVariable = 0;
    char * id = malloc(sizeof(memberId));
    sprintf(id, "%d", memberId);
    conn = mysql_init(NULL);

    MYSQL_RES *res;
    MYSQL_ROW row;

    /* Connect to database */
   if (!mysql_real_connect(conn, server,
         user, password, database, 3307, NULL, 0)) {
      fprintf(stderr, "%s\n", mysql_error(conn));
      return(0);
   }

    /* send SQL query */
    char * loanStatusQuery = "SELECT * FROM loan WHERE memberId = '%s'";
    char query[strlen(id) + sizeof loanStatusQuery];
    printf("%s\n", id);
    //remove new line character
    sprintf(query, loanStatusQuery, id);
   if (mysql_query(conn, query)) {
      fprintf(stderr, "%s\n", mysql_error(conn));
      return(0);
   }

   res = mysql_use_result(conn);
   
   if((row = mysql_fetch_row(res)) != NULL){
        loanStatusVariable = 1;
   }

    // while ((row = mysql_fetch_row(res)) != NULL)
    // printf("%s %s\n", row[1], row[2]);

   /* Release memory used to store results and close connection */
    mysql_free_result(res);
    mysql_close(conn);
    return loanStatusVariable;
}

int totalContributions(){
    totalContributionVariable = 0;
    char * id = malloc(sizeof(memberId));

    //copy int id to char * variable
    sprintf(id, "%d", memberId);
    conn = mysql_init(NULL);

    MYSQL_ROW row;
    MYSQL_FIELD *field; 

    /* Connect to database */
   if (!mysql_real_connect(conn, server,
         user, password, database, 3307, NULL, 0)) {
      fprintf(stderr, "%s\n", mysql_error(conn));
      return(0);
   }

    /* send SQL query */
    char * query_template = "SELECT * FROM contribution WHERE memberID = '%s'";
    char query[strlen(id) + sizeof query_template];

    sprintf(query, query_template, id);
   if (mysql_query(conn, query)) {
      fprintf(stderr, "%s\n", mysql_error(conn));
      return(0);
   }

   //get results from connection output
   MYSQL_RES *result = mysql_store_result(conn);
   if (result == NULL) 
    {
      fprintf(stderr, "%s\n", mysql_error(conn));
      return(0);
    }

    int num_fields = mysql_num_fields(result);
    unsigned int name_field;
    char *field_name = "amount";

    char *headers[num_fields];
    for(unsigned int i = 0; (field = mysql_fetch_field(result)); i++) {
        headers[i] = field->name;
        if (strcmp(field_name, headers[i]) == 0) {
            name_field = i;
        }
    }

    while ((row = mysql_fetch_row(result))) {
        //do something with row[name_field]
        if(row[name_field] != NULL){
            totalContributionVariable += (int) strtol(row[name_field], NULL, 10);
        }
    }

   /* Release memory used to store results and close connection */
    mysql_free_result(result);
    mysql_close(conn);
    return totalContributionVariable;
}

int totalBenefits(){
    totalContributionVariable = 0;
    char * id = malloc(sizeof(memberId));

    //copy int id to char * variable
    sprintf(id, "%d", memberId);
    conn = mysql_init(NULL);

    MYSQL_ROW row;
    MYSQL_FIELD *field; 

    /* Connect to database */
   if (!mysql_real_connect(conn, server,
         user, password, database, 3307, NULL, 0)) {
      fprintf(stderr, "%s\n", mysql_error(conn));
      return(0);
   }

    /* send SQL query */
    char * query_template = "SELECT * FROM benefits WHERE memberID = '%s'";
    char query[strlen(id) + sizeof query_template];

    sprintf(query, query_template, id);
   if (mysql_query(conn, query)) {
      fprintf(stderr, "%s\n", mysql_error(conn));
      return(0);
   }

   //get results from connection output
   MYSQL_RES *result = mysql_store_result(conn);
   if (result == NULL) 
    {
      fprintf(stderr, "%s\n", mysql_error(conn));
      return(0);
    }

    int num_fields = mysql_num_fields(result);
    unsigned int name_field;
    char *field_name = "gainedProfits";

    char *headers[num_fields];
    for(unsigned int i = 0; (field = mysql_fetch_field(result)); i++) {
        headers[i] = field->name;
        if (strcmp(field_name, headers[i]) == 0) {
            name_field = i;
        }
    }

    while ((row = mysql_fetch_row(result))) {
        //do something with row[name_field]
        if(row[name_field] != NULL){
            totalContributionVariable += (int) strtol(row[name_field], NULL, 10);
        }
    }

   /* Release memory used to store results and close connection */
    mysql_free_result(result);
    mysql_close(conn);
    return totalContributionVariable;
}

int getLoanPaymentDetails(){
    totalLoanPayBack = 0;
    char * id = malloc(sizeof(memberId));

    //copy int id to char * variable
    sprintf(id, "%d", memberId);
    conn = mysql_init(NULL);

    MYSQL_ROW row;
    MYSQL_FIELD *field; 

    /* Connect to database */
   if (!mysql_real_connect(conn, server,
         user, password, database, 3307, NULL, 0)) {
      fprintf(stderr, "%s\n", mysql_error(conn));
      return(0);
   }

    /* send SQL query */
    char * query_template = "SELECT amount, interest FROM loan WHERE memberId = '%s'";
    char query[strlen(id) + sizeof query_template];

    sprintf(query, query_template, id);
   if (mysql_query(conn, query)) {
      fprintf(stderr, "%s\n", mysql_error(conn));
      return(0);
   }

   //get results from connection output
   MYSQL_RES *result = mysql_store_result(conn);
   if (result == NULL) 
    {
      fprintf(stderr, "%s\n", mysql_error(conn));
      return(0);
    }

    int num_fields = mysql_num_fields(result);
    unsigned int amount_field, interest_field;
    char *amountColumn = "amount";
    char *interestColumn = "interest";

    char *headers[num_fields];
    for(unsigned int i = 0; (field = mysql_fetch_field(result)); i++) {
        headers[i] = field->name;
        if (strcmp(amountColumn, headers[i]) == 0) {
            amount_field = i;
        }
        if (strcmp(interestColumn, headers[i]) == 0) {
            interest_field = i;
        }
    }

    while ((row = mysql_fetch_row(result))) {

        if(row[amount_field] != NULL && row[interest_field]!= NULL){
            actualBorrowedLoan = (int) strtol(row[amount_field], NULL, 10);
            totalLoanPayBack = (int) strtol(row[interest_field], NULL, 10) + (int) strtol(row[amount_field], NULL, 10);
        }
    }

   /* Release memory used to store results and close connection */
    mysql_free_result(result);
    mysql_close(conn);
    return totalLoanPayBack;
}

/*Handles client response on the initial menu*/
void handleClientResponse(char clientMessage[1024], int socketS){

    char * sentString = clientMessage;

    if(strcmp(clientMessage, "contribution check") == 0){
        if(totalContributions() != 0){
            char * yourContributionString = "Your contribution amount is : \t";
            char * contributionAmount = malloc(sizeof(totalContributionVariable));
            
            //copy int id to char * variable
            sprintf(contributionAmount, "%d", totalContributionVariable);
            char * serverResponseStringOne = concatinateString(yourContributionString, contributionAmount);
            char * serverResponseStringFinal = concatinateString(serverResponseStringOne, " UGX \nEnter yes to enter new command");
            //char serverResponse [1024] = "Contribution \t\t Amount in UGX\n Your contribution \t 200,000UGX\n Total Contribution \t 6,700,000UGX\nEnter Yes to show menu or No to exit\n";
            clientSender(serverResponseStringFinal, socketS);
        }else{
            char serverResponse [1024] = "You have no contributions!\n\nEnter yes to enter new command\n\n";
            clientSender(serverResponse, socketS);
        }
       

    }else if (strcasestr(sentString,"contribution")){
        
        //assuming user selected adding new contribution
        handleContributionAdd(sentString, socketS);

    }else if(strcmp(clientMessage, "benefits check") == 0){
        if(totalBenefits() != 0){
            char * yourBenefitsString = "Your benefits amount is : \t";
            char * benefitsAmount = malloc(sizeof(totalContributionVariable));
            
            //copy int id to char * variable
            sprintf(benefitsAmount, "%d", totalContributionVariable);
            char * serverResponseStringOne = concatinateString(yourBenefitsString, benefitsAmount);
            char * serverResponseStringFinal = concatinateString(serverResponseStringOne, " UGX \nEnter yes to enter new command");
            //char serverResponse [1024] = "Contribution \t\t Amount in UGX\n Your contribution \t 200,000UGX\n Total Contribution \t 6,700,000UGX\nEnter Yes to show menu or No to exit\n";
            clientSender(serverResponseStringFinal, socketS);
        }else{
            char serverResponse [1024] = "You have no benefits!";
            clientSender(serverResponse, socketS);
        }

    }else if(strcasestr(sentString, "loan request")){

        //assuming user is requesting for a loan
        handleLoanRequest(sentString, socketS);
        
    }else if(strcmp(clientMessage, "loan status") == 0){
        if(loanStatus() == 0){
            char serverResponse [1024] = "Your loan is still pending.\nEnter Yes to show menu or No to exit\n";
            clientSender(serverResponse, socketS);
        }else{
            char serverResponse [1024] = "Your loan is has been approved.\nEnter Yes to show menu or No to exit\n";
            clientSender(serverResponse, socketS);
        }
        
    }else if (strcasestr(sentString, "loan payment")){
        handleLoanPayment(sentString,socketS);

    }else if(strcmp(clientMessage, "loan repayment_details") == 0){
        getLoanPaymentDetails();
        if(totalLoanPayBack != 0){
            int paymentPerMonth = totalLoanPayBack / 12 ;
            char * paymentPerMonthString = malloc(sizeof(paymentPerMonth));
            sprintf(paymentPerMonthString, "%d", paymentPerMonth);

            char * yourLoanDetailsString = "Loan payable amount is : \t";
            char * yourLoanTotal = malloc(sizeof(totalLoanPayBack));
            
            //copy int id to char * variable
            sprintf(yourLoanTotal, "%d", totalLoanPayBack);
            char * serverResponseStringOne = concatinateString(yourLoanDetailsString, yourLoanTotal);
            char * serverResponseStringTwo = concatinateString(serverResponseStringOne, " UGX \n");
            char * serverResponseStringThree = concatinateString(serverResponseStringTwo, "You will be paying \t");
            char * serverResponseStringFour = concatinateString(serverResponseStringThree, paymentPerMonthString);
            char * serverResponseStringFinal = concatinateString(serverResponseStringFour, " UGX per Month\n Enter yes to continue\n");
            //char serverResponse [1024] = "Contribution \t\t Amount in UGX\n Your contribution \t 200,000UGX\n Total Contribution \t 6,700,000UGX\nEnter Yes to show menu or No to exit\n";
            clientSender(serverResponseStringFinal, socketS);
        }else{
             optionHandler = optionLoanPaymentDetails;
            char serverResponse [1024] = "You have no active loan.\nEnter Yes to show menu or No to exit\n";
            clientSender(serverResponse, socketS);
        }
    }else if(strcasestr(sentString,"idea")){

        //adding idea to temp file
        handleIdeaAdd(sentString, socketS);
        
    }else if(strcasestr(sentString, "help")){
        char serverResponse [4096] = "To enter contribution, enter : \ncontribution amount date person_name receipt_number \ne.g contribution 5000 12/07/2017 Roland receipt1200 \n\nTo check for contributions, type: contribution check \n\nTo check for benefits, type: benefits check \n\nTo request loan, type: loan request amount. e.g loan request 3000\n\nTo check loan status, type: loan status\n\nTo check loan repayment_details, type: loan repayment_details\n\nTo add an investment idea, type: \nidea name capital 'simple idea description'\ne.g idea charocalBuring 40000 'from mabira forest'\n\nEnter yes to continue\n\n";
        clientSender(serverResponse, socketS);
    }else{
        optionHandler = optionDefault;
        clientSender(initiation_message, socketS);
    }

}

void fetchFromDatabase(char command[]){

}

/*Handles contribution addition*/
void handleContributionAdd(char contributionString[1024], int socketS){
    char serverResponse [1024] = "Added contribution successfully, please check later for approval. Thank you\n Enter Yes to show menu or No to exit\n";

    char * userId = malloc(sizeof(memberId));
    //copy int id to char * variable
    sprintf(userId, "%d", memberId);

    char * enhanceOne = concatinateString(contributionString, " ");
    char * enhanceTwo = concatinateString(enhanceOne, userId);
    char * enhnaceThree = concatinateString(enhanceTwo, " SACCO");

    //save contribution
    addContributionToTempFile(enhnaceThree);

    //set option handler back to zero
    optionHandler = "";
    clientSender(serverResponse, socketS);
}

/*Handles contribution addition*/
void handleIdeaAdd(char ideaString[2048], int socketS){
    char serverResponse [1024] = "Added idea successfully. Thank you\n Enter Yes to show menu or No to exit\n";

    //save idea
    char * userId = malloc(sizeof(memberId));
    //copy int id to char * variable
    sprintf(userId, "%d", memberId);

    char * enhanceOne = concatinateString(ideaString, " ");
    char * enhanceTwo = concatinateString(enhanceOne, userId);
    char * enhnaceThree = concatinateString(enhanceTwo, " SACCO");

    addIdeaToTempFile(enhnaceThree);

    //set option handler back to zero
    optionHandler = "";
    clientSender(serverResponse, socketS);
}

/*Handles loan request*/
void handleLoanRequest(char loanString[1024], int socketS){
    char serverResponse [1024] = "Loan Requested successfully. Pending approval. \n Enter Yes to show menu or No to exit\n";

    //save loan
    char * userId = malloc(sizeof(memberId));
    //copy int id to char * variable
    sprintf(userId, "%d", memberId);

    char * enhanceOne = concatinateString(loanString, " ");
    char * enhanceTwo = concatinateString(enhanceOne, userId);
    char * enhnaceThree = concatinateString(enhanceTwo, " SACCO");

    addLoanToTempFile(enhnaceThree);

    //set option handler back to zero
    optionHandler = "";
    clientSender(serverResponse, socketS);
}

void handleLoanPayment(char loanPaymentString[1024], int socketS){
    char serverResponse [1024] = "Loan month fees successfully paid. \n Enter Yes to show menu or No to exit\n";

    //set option handler back to zero
    optionHandler = "";
    clientSender(serverResponse, socketS);
}

/*This sends messages back to user*/
void clientSender(char message[1024], int socketS){
    send(socketS , message , strlen(message) , 0 );
}

/*This method creates a file on the server to store temp data 
*/
void addContributionToTempFile(char data[1024]){
    //meaning we are creating a file for sacco contribution, temp file as is in question
    FILE * fPointer;
    fPointer = fopen("/Applications/XAMPP/xamppfiles/htdocs/Sacco/contributions.txt","a");
    fprintf(fPointer, "%s\n", data);
    fclose(fPointer);
}

/*This method creates a file on the server to store temp data 
*/
void addIdeaToTempFile(char data[1024]){
    //meaning we are creating a file for sacco idea, temp file as is in question
    FILE * fPointer;
    fPointer = fopen("/Applications/XAMPP/xamppfiles/htdocs/Sacco/ideas.txt","a");
    fprintf(fPointer, "%s\n", data);
    fclose(fPointer);
}

/*This method creates a file on the server to store temp data 
*/
void addLoanToTempFile(char data[1024]){
    //meaning we are creating a file for sacco loan, temp file as is in question
    FILE * fPointer;
    fPointer = fopen("/Applications/XAMPP/xamppfiles/htdocs/Sacco/loans.txt","a");
    fprintf(fPointer, "%s\n", data);
    fclose(fPointer);
}

