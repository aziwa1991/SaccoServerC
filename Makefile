CC = gcc
INCLUDES = -I/usr/local/mysql-5.7.19-macos10.12-x86_64/include/mysql
LIBS = -L/usr/local/mysql-5.7.19-macos10.12-x86_64/lib -lmysqlclient

all: server

main.o: main.c
	$(CC) -c $(INCLUDES) main.c

server: main.o
	$(CC) -o server main.o $(LIBS)
clean:
	rm -f server main.o